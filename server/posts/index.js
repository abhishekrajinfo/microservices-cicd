const express = require("express");
const bodyParser = require("body-parser");
const { randomBytes } = require("crypto");
const cors = require("cors");
const axios = require("axios");

const app = express();
app.use(bodyParser.json());
app.use(cors());

const posts = [];

app.get("/posts", (req, res) => {
  res.send(posts);
});

app.post("/posts", async (req, res) => {
  const id = randomBytes(4).toString("hex");
  const { title } = req.body;

  posts.push({
    id,
    title,
  });

  // Send event to event-bus
  await axios.post("http://localhost:4005/events", {
    type: "PostCreated",
    data: {
      id,
      title,
    },
  });

  res.send(200);
});

app.post("/events", (req, res) => {
  console.log("Received Event", req.body.type);

  res.status(200).send(posts);
});

app.listen(4000, () => {
  console.log("Listening on 4000");
});
